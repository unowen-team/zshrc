setopt no_beep              # Don't beep.
setopt ignore_eof           # Don't quit on Ctrl+D.
setopt interactive_comments # Allow comments on cmd line.
setopt multios              # Allow multiple redirection echo 'a'>b>c.
unsetopt flow_control       # Disable start/stop characters in shell editor.
unsetopt clobber            # Stricter > and >>. # Use >! and >>! to bypass.
unsetopt bg_nice            # Don't run all background jobs at a lower priority.