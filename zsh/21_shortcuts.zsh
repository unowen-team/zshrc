if [[ "$TERM" == 'dumb' ]]; then
    return 1
fi

WORDCHARS='*?_-.[]~&;!#$%^(){}<>'

# Allow command line editing in an external editor.
autoload -Uz edit-command-line
zle -N edit-command-line

function prepend-sudo {
    if [[ "$BUFFER" != su(do|)\ * ]]; then
        BUFFER="sudo $BUFFER"
        (( CURSOR += 5 ))
    fi
}
zle -N prepend-sudo

bindkey -e # Use emacs key bindings

bindkey '^[[3~'   delete-char            # del
bindkey '^[[2~'   overwrite-mode         # ins
bindkey '^[[H'    beginning-of-line      # home
bindkey '^[[F'    end-of-line            # end
bindkey '^[[6~'   down-line-or-history   # pgdown
bindkey '^[[5~'   up-line-or-history     # pgup
bindkey '^[[1;5D' backward-word          # ctrl+left
bindkey '^[[1;5C' forward-word           # ctrl+right
bindkey '^w'      backward-kill-word     # ctrl+w, alt+backspace
bindkey '^[[3;5~' kill-word              # ctrl+del
bindkey '^b'      undo                   # ctrl+b
bindkey '^L'      clear-screen           # ctrl+l
bindkey '^r'      history-incremental-search-backward # ctrl+r
bindkey '^X^E'    edit-command-line      # ctrl+x+e
bindkey '^[[Z'    reverse-menu-complete  # shift+tab
bindkey '^[^[[D'  backward-word          # alt+left
bindkey '^[^[[C'  forward-word           # alt+right
bindkey '^[s'     prepend-sudo           # alt+s
bindkey "^[m"     copy-prev-shell-word   # alt+m