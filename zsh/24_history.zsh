HISTFILE=~/.local/share/zsh.histfile
HISTSIZE=10000
SAVEHIST=$HISTSIZE

setopt extended_history    # Write the history file in the ':start:elapsed;command' format.
setopt inc_append_history  # Write to the history file immediately, not when the shell exits.
setopt hist_ignore_space   # Do not record an command line starting with a space.
setopt hist_verify         # On history expansion, don't execute the line, but rather reload the it into the editing buffer.
