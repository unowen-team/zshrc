if [[ "$TERM" == 'dumb' ]]; then
    return 1
fi

# Add custom completions to path
fpath=(~/.config/zsh/completion $fpath)

# Load & init
autoload -Uz compinit
compinit -d ~/.cache/zsh/zcompdump

# Options
setopt complete_in_word    # Do not move cursor to word end on completion start.
setopt always_to_end       # Move cursor to the end of a completed word (seems to not work).
unsetopt menu_complete     # Do not autoselect the first completion entry.

# Use caching to make completion for cammands such as dpkg and apt usable.
zstyle ':completion::complete:*' use-cache true
zstyle ':completion::complete:*' hosts off
zstyle ':completion::complete:*' cache-path ~/.cache/zsh/zcompcache

# Completions case sensitivity
#zstyle ':completion:*' matcher-list 'r:|[._-]=* r:|=*' 'l:|=* r:|=*' # case sensitive
zstyle ':completion:*' matcher-list 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=*' 'l:|=* r:|=*' # case insensitive

# Group matches and describe. Some of these I have no idea about.
zstyle ':completion:*' menu select
zstyle ':completion:*' list-prompt '%S%M matches%s'
zstyle ':completion:*' select-prompt '%S%M matches%s'
zstyle ':completion:*' group-name ''
zstyle ':completion:*' list-colors ${(s.:.)LS_COLORS}
#zstyle ':completion:*:-command-:*:(commands|builtins|reserved-words-aliases)' group-name commands
#zstyle ':completion:*' original true
#zstyle ':completion:*' remote-access false
zstyle ':completion:*' verbose true
zstyle ':completion:*' squeeze-slashes true
zstyle ':completion:*' format '%U%F{yellow}%d%f%u'
zstyle ':completion:*:corrections'  format  '%F{green}%d (errors: %e)%f'
zstyle ':completion:*:descriptions' format  '%F{yellow}%d%f'
zstyle ':completion:*:messages'     format  '%U%F{purple}%d%f%u'
zstyle ':completion:*:warnings'     format $'%U%F{red}no matches found%f%u\nSearched: %d'

# Don't complete uninteresting users...
zstyle ':completion:*:*:*:users' ignored-patterns \
    adm amanda apache avahi beaglidx bin cacti canna clamav daemon \
    dbus distcache dovecot fax ftp games gdm gkrellmd gopher \
    hacluster haldaemon halt hsqldb ident junkbust ldap lp mail \
    mailman mailnull mldonkey mysql nagios \
    named netdump news nfsnobody nobody nscd ntp nut nx openvpn \
    operator pcap postfix postgres privoxy pulse pvm quagga radvd \
    rpc rpcuser rpm shutdown squid sshd sync uucp vcsa xfs '_*'

# Ignore multiple entries.
zstyle ':completion:*:(rm|kill|diff):*' ignore-line other
zstyle ':completion:*:rm:*' file-patterns '*:all-files'

# Kill
zstyle ':completion:*:processes' command 'ps -u $USER -o pid,user,command -w'
zstyle ':completion:*:processes-names' command 'ps -e -o comm='
zstyle ':completion:*:*:kill:*' menu yes select
zstyle ':completion:*:*:killall:*' menu yes select

# Man
zstyle ':completion:*:manuals' separate-sections true
zstyle ':completion:*:manuals.(^1*)' insert-sections true