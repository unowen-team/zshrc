type docker >/dev/null 2>&1
if [ $? -eq 0 ]; then
    function dphp70() {
        cwd=$(docker_fixpath $(pwd))
        args=$@
        docker exec -it dockerweb_php70_1 /bin/sh -c "mkdir -p /var/www; chown www-data:www-data /var/www/; cd $cwd && su www-data -s /bin/sh -c 'php $args'"
    }
    function dphp() {
        function docker_fixpath {
            arg="$1"
            echo ${arg/$HOME\/Projects/\/projects}
        }
        
        args=()
        if [ $# -gt 0 ]; then
            for i in {1..$#}; do
                arg=$@[i]
                args[i]=\""$(docker_fixpath $arg)"\"
            done
        fi
        dphp70 $args
    }
    function dcomposer() {
        dphp ~/Projects/PHP/composer.phar $@
    }
fi
