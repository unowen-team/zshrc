case $TERM in
*xterm*)
    term_title_precmd () {
        print -Pn "\e]0;%n@%M:%1~\a"
    } # %d : %n
    term_title_preexec () {
        print -Pn "\e]0;%n@%M:%1~ %# $1\a"
    }

    add-zsh-hook precmd term_title_precmd
    add-zsh-hook preexec term_title_preexec
    ;;
screen)
    term_title_precmd () {
        print -Pn "\e]83;title \"$1\"\a"
        print -Pn "\e]0;$TERM - (%L) [%n@%M]%# [%~]\a"
    }
    term_title_preexec () {
        print -Pn "\e]83;title \"$1\"\a"
        print -Pn "\e]0;$TERM - (%L) [%n@%M]%# [%~] ($1)\a"
    }

    add-zsh-hook precmd term_title_precmd
    add-zsh-hook preexec term_title_preexec
    ;;
esac

