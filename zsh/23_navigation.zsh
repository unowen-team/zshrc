setopt auto_pushd         # Push the old directory onto the stack on cd.
setopt pushd_ignore_dups  # Do not store duplicates in the stack.
setopt pushd_minus        # Exchanges the meanings of ‘+’ and ‘-’ when used with a number to specify a directory in the stack.
setopt pushd_silent       # Do not print the directory stack after pushd or popd.