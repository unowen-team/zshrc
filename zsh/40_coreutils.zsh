alias ls='ls --color=auto --group-directories-first --color=auto'
alias ltr='ls -ltr'
alias grep='grep --color=auto'