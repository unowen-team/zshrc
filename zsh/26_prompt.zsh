setopt prompt_subst
autoload -U add-zsh-hook
autoload -U promptinit
autoload -Uz vcs_info
autoload colors
colors
promptinit

#use extended color pallete if available
if [[ $TERM = *256color* || $TERM = *rxvt* ]]; then
    turquoise="%F{81}"
    orange="%F{166}"
    purple="%F{135}"
    hotpink="%F{161}"
    limegreen="%F{118}"
else
    turquoise="$fg[cyan]"
    orange="$fg[yellow]"
    purple="$fg[magenta]"
    hotpink="$fg[red]"
    limegreen="$fg[green]"
fi

# vcs
FMT_FORMATS="(%{$turquoise%}%b%u%c%{$reset_color%})"
FMT_FORMATS_UNTRACKED="(%{$turquoise%}%b%{$hotpink%}●%u%c%{$reset_color%})"
FMT_ACTION="(%{$limegreen%}%a%{$reset_color%})"
zstyle ':vcs_info:*' enable git svn
zstyle ':vcs_info:*:prompt:*' get-revision true
zstyle ':vcs_info:*:prompt:*' check-for-changes true
zstyle ':vcs_info:*:prompt:*' unstagedstr   "%{$orange%}●"
zstyle ':vcs_info:*:prompt:*' stagedstr     "%{$limegreen%}●"
zstyle ':vcs_info:*:prompt:*' formats       "${FMT_FORMATS}"
zstyle ':vcs_info:*:prompt:*' actionformats "${FMT_FORMATS}${FMT_ACTION}"
zstyle ':vcs_info:*:prompt:*' nvcsformats   ""

vcs_info_precmd() {
    if git ls-files --other --exclude-standard 2> /dev/null | grep -q "."; then
        zstyle ':vcs_info:*:prompt:*' formats "${FMT_FORMATS_UNTRACKED}"
        zstyle ':vcs_info:*:prompt:*' actionformats "${FMT_FORMATS_UNTRACKED}${FMT_ACTION}"
    else
        zstyle ':vcs_info:*:prompt:*' formats "${FMT_FORMATS}"
        zstyle ':vcs_info:*:prompt:*' actionformats "${FMT_FORMATS}${FMT_ACTION}"
    fi
    vcs_info 'prompt'
}
add-zsh-hook precmd vcs_info_precmd

export VIRTUAL_ENV_DISABLE_PROMPT=1
function virtualenv_info {
    [ $VIRTUAL_ENV ] && echo '('$fg[blue]`basename $VIRTUAL_ENV`%{$reset_color%}') '
}

PROMPT=$'%(?.%{$fg[gray]%}.%{$fg[red]%})<%?> %{$fg[default]%}\n\n%{$purple%}%n%{$reset_color%} at %{$orange%}%m%{$reset_color%} in %{$limegreen%}%~%{$reset_color%} $vcs_info_msg_0_$(virtualenv_info)%{$reset_color%}\n$ '
RPROMPT=$'%{$fg[gray]%}%D %*'
