# Shortcuts I forget

| Shortcut      | Hint                               |
| :---          | :---                               |
| `ctrl+u`      | kill-line                          |
| `ctrl+o`      | repeat & advance command           |
| `ctrl+r`      | incrementa history search          |
| `ctrl+b`      | undo (or ctrl+/)                   |
| `ctrl+l`      | clear screen                       |
| `ctrl+del`    | kill-word                          |
| `ctrl+x+e`    | edit cmd in $EDITOR                |
| `alt+p`       | find command that starts like this |
| `alt+h`       | help                               |
| `alt+q`       | store command till next            |
| `alt+a`       | execute and retype command         |
| `alt+s`       | prepend sudo                       |
| `alt+m`       | copy word to the left              |
| `alt+backspc` | backward-kill-word                 |

# Commads I forget

| Command                 | Hint                              |
| :---                    | :---                              |
| `fc -RI`                | Import history from another shell |
| `history 0`             | List all recoreded history        |
| `dirs -v` or `cd+<Tab>` | Pushd history                     |

# Notes to future self

 * man zshoptions
 * http://zsh.sourceforge.net/Doc/Release/Options.html
 * http://zsh.sourceforge.net/Guide/zshguide.html
 * http://zsh.sourceforge.net/Doc/Release/Completion-System.html
 * https://github.com/sorin-ionescu/prezto
 * https://github.com/mattfoster/zshkit
 * https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/themes/steeef.zsh-theme
 * https://github.com/zsh-users/zsh-completions/tree/master/src
 * http://pwet.fr/man/linux/commandes/zsh_lovers
 * http://stackoverflow.com/questions/171563/whats-in-your-zshrc
 * http://forums.gentoo.org/viewtopic-t-827362-start-0.html
 * https://developer.apple.com/library/mac/#documentation/Darwin/Reference/ManPages/man1/zshcompsys.1.html
 * http://eseth.org/2010/hg-in-zsh.html
 * http://askql.wordpress.com/2011/01/11/zsh-writing-own-completion/
